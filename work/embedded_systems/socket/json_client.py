#!/usr/bin/python3
 
import httplib2
import urllib
import json
import sys

#   '''{"operation": "add"},'''
message = '''
{
   "operation": "add",
   "val1": 34,
   "val2": 56
}
'''

def client(ip='127.0.0.1', port=8008):
   try:
      url     = 'http://127.0.0.1:8008'   
      headers = {'Content-type': 'application/json; charset=UTF-8'}
      data    = json.loads(message)

      #print (data['operation'])

      http = httplib2.Http()
      response, content = http.request(url, 'POST', headers=headers, body=json.dumps(data))
      #print ('response=', response)
      #print ('content=', content)

      data = json.loads(content)
      if data['received'] == 'ok':
          print ("result =", data['result'], ", operation =", data['operation'], ", val1 =", data['val1'], ", val2 =", data['val2'])   
  
         


      http = httplib2.Http()
      response, content = http.request("http://127.0.0.1:8008", "GET")
      #print ('response=', response)
      #print ('content=', content)
    


   except:
      print("Oops!",sys.exc_info()[0],"occurred.")


'''
http = httplib2.Http()
#content = http.request("http://www.test.ch","HEAD")[1]
#content = http.request("http://perdu.com")[1]
content = http.request("http://127.0.0.1:8008", "GET")[1]
'''


if __name__ == "__main__":
   client()
