#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>

// Prototype functions
static void catchSignal      (int signal);
static void createCatchSignal(void);

static int  pipe_fd;
extern int  errno;







#define NAMED_PIPE "/tmp/myFifo"
#define PIPE_BUF 1024

int main(void)
{
int   nbBytes;
int   val;
char  buf[32];


    createCatchSignal ();


    if (access (NAMED_PIPE, F_OK) == -1)
        mkfifo (NAMED_PIPE, 0777);


    for ( ; ; )
    {
        memset (&buf[0], 0, sizeof(buf));
        pipe_fd = open(NAMED_PIPE, O_RDONLY);
        for (nbBytes=sizeof(buf); nbBytes;)
        {
            nbBytes = read (pipe_fd, &buf[0], sizeof(buf));
            printf ("nbBytes=%d\n", nbBytes);
            printf ("%s\n", &buf[0]);
        }
        close (pipe_fd);

        printf ("Enter an integer \n");
        scanf ("%d", &val);
        printf ("val=%d\n", val);

    }

}


static void createCatchSignal(void)
{
struct sigaction act;

    act.sa_handler = catchSignal;
    sigemptyset (&act.sa_mask);
    act.sa_flags   = 0;
    sigaction (SIGINT,  &act, 0);
    sigaction (SIGTSTP, &act, 0);
    sigaction (SIGTERM, &act, 0);
    sigaction (SIGABRT, &act, 0);
}


static void catchSignal (int signal)
{
    printf ("signal = %d\n", signal);
    //close (pipe_fd);
    pthread_exit (NULL);

}


