//http://www.netzmafia.de/skripten/unix/linux-daemon-howto.html

#include <stdio.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>

static void setDaemon        (void);
static void catchSignal      (int signal);
static void createCatchSignal(void);

static FILE *pFile;


int main (int argc, char **argv)
{
int   i;

    createCatchSignal ();

    openlog(argv[0], LOG_PID | LOG_NDELAY, LOG_DAEMON);
    syslog(LOG_INFO, "Start 1");


    setDaemon();
    
    pFile = fopen ("/t.txt", "w");
    for (i=0;;i++)
    {
        fprintf(pFile,"increment=%d\n",i);
        fflush (pFile);
        sleep(1);
    }
    exit(EXIT_SUCCESS);
}

static void  setDaemon(void)
{ 
pid_t pid, sid;

    
    
    pid = fork();                               // Fork off the parent process
    
    if (pid < 0) {
       exit(EXIT_FAILURE);
    }
    
    if (pid > 0) {                              // If we got a good PID, 
       exit(EXIT_SUCCESS);                      // then we can exit the parent process.
    }

    umask(0027);                                // Change the file mode mask
  
   
    sid = setsid();                             // Create a new SID (link with tty) for the child process
    if (sid < 0) {
        syslog(LOG_ERR, "setsid error=%d", sid);
        exit(EXIT_FAILURE);
    }

      
    
    if ((chdir("/home/test/daemon")) < 0) {     // Change the current working directory
        syslog(LOG_ERR, "chdir error");
        exit(EXIT_FAILURE);
    }

    if (chroot(".")) {                          // Change the root directory
        syslog(LOG_ERR, "chroot error");
        exit(EXIT_FAILURE);
    }

    if ((setegid(1002)) != 0) {                 // Change the effective (and real) group ID (test)
        syslog(LOG_ERR, "setegid error");
        exit(EXIT_FAILURE);
    }
    if ((seteuid(1002)) != 0) {                 // Change the effective (and real) user  ID (test)
        syslog(LOG_ERR, "seteuid error");
        exit(EXIT_FAILURE);
    }
  
  

    
    close(STDIN_FILENO);                        // Close the standard file descriptors
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    syslog(LOG_INFO, "Start OK");
}


static void createCatchSignal(void)
{
struct sigaction act;

    memset (&act, 0, sizeof (act));
    act.sa_handler = catchSignal;
    sigaction (SIGTERM, &act, 0);  // kill PID
}


static void catchSignal (int signal)
{
    fclose (pFile);
    syslog(LOG_INFO, "Stop OK");
    exit(EXIT_SUCCESS);
}





