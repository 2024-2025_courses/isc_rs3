#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>



static void catchSignal      (int signal);
static void createCatchSignal(void);


static pthread_t thr_1_ID;
static pthread_t thr_2_ID;
static pthread_t thr_3_ID;


void *thr_1 (void *arg);
void *thr_2 (void *arg);
void *thr_3 (void *arg);


sem_t semaphore1;
sem_t semaphore2;
sem_t semaphore3;


int main (void)
{
    void *returnMessage;

    createCatchSignal ();


    
    sem_init (&semaphore1, 0, 0);
    sem_init (&semaphore2, 0, 0);
    sem_init (&semaphore3, 0, 1);
    
    pthread_create (&thr_1_ID, NULL, thr_1, NULL);
    pthread_create (&thr_2_ID, NULL, thr_2, NULL);
    pthread_create (&thr_3_ID, NULL, thr_3, NULL);
    pthread_join (thr_1_ID, &returnMessage);
    pthread_join (thr_2_ID, &returnMessage);
    pthread_join (thr_3_ID, &returnMessage);

    printf ("destroy=%d\n", sem_destroy(&semaphore1));
    printf ("destroy=%d\n", sem_destroy(&semaphore2));
    printf ("destroy=%d\n", sem_destroy(&semaphore3));
    printf ("Stop\n");


    exit (EXIT_SUCCESS);
}


static void createCatchSignal(void)
{
struct sigaction act;

    act.sa_handler = catchSignal;
    sigemptyset (&act.sa_mask);
    act.sa_flags   = 0;
    sigaction (SIGINT,  &act, 0);
    sigaction (SIGTSTP, &act, 0);
    sigaction (SIGTERM, &act, 0);
    sigaction (SIGABRT, &act, 0);
}


static void catchSignal (int signal)
{
    printf ("signal = %d\n", signal);
    pthread_cancel(thr_1_ID);
    pthread_cancel(thr_2_ID);
    pthread_cancel(thr_3_ID);
}

