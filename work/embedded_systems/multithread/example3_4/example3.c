//  gcc -Wall -D_REENTRANT -o example3 example3.c -lpthread


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void *thr_funct1 (void *arg);
void *thr_funct2 (void *arg);

int main (void) {
pthread_t thread_ID1, thread_ID2;

    pthread_create (&thread_ID1, NULL, thr_funct1, NULL);
    pthread_create (&thread_ID2, NULL, thr_funct2, NULL);

    pthread_join (thread_ID1, NULL);
    pthread_join (thread_ID2, NULL);

    exit (EXIT_SUCCESS);
}

void* thr_funct1 (void *arg) {
    sleep (2);
    pthread_exit ("Thread1 end\n");
}



void *thr_funct2 (void *arg) {
    sleep (4);
    pthread_exit ("Thread2 end\n");
}

