#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include <unistd.h>



#define K1     "0"
#define K2     "2"
#define K3     "3"

#define LED0   "10"
#define LED1  "362"

#define GPIO_EXPORT    "/sys/class/gpio/export"
#define GPIO_UNEXPORT  "/sys/class/gpio/unexport"

#define GPIO_PATH      "/sys/class/gpio/gpio"
#define GPIO_DIRECTION "/direction"
#define GPIO_VALUE     "/value"
#define IN             "in"
#define OUT            "out"


int main (void)
{
int   fd;
char  file [128];
int   val;
int   nbByte;
unsigned char input;
char  *pInput  = "2";
char  *pOutput = "10";
char  *pDir;


    // Read K2
    //-------------------------------------------------------------------------
    // activate input K2 (="2")
    fd = open("/sys/class/gpio/export",O_WRONLY); //  
    write (fd, pInput, strlen(pInput));
    close (fd);

    // indicate direction = in
    memset (file, 0, sizeof(file));
    strcat (file, "/sys/class/gpio/gpio");
    strcat (file, pInput);
    strcat (file, "/direction");  //  file = "/sys/class/gpio/gpio2/direction"
    pDir = "in";
    fd = open(file,O_RDWR);
    write (fd, pDir, strlen(pDir));
    close (fd);


    // Read K2
    memset (file, 0, sizeof(file));
    strcat (file, "/sys/class/gpio/gpio");
    strcat (file, pInput);
    strcat (file, "/value"); // file = "/sys/class/gpio/gpio2/value"

    val=0;
    fd = open(file,O_RDONLY);
    nbByte=pread (fd, &val, sizeof(val), 0);
    close (fd);
    input = (unsigned char) val - '0';
    printf ("Read K2: nbByte=%d, val=0x%x, input=%d\n", nbByte, val, input);

    // Deactivate K2
    fd = open("/sys/class/gpio/unexport",O_WRONLY); //  "/sys/class/gpio/unexport"
    write (fd, pInput, strlen(pInput));
    close (fd);



    // Write LED0
    //-------------------------------------------------------------------------
    // activate output LED0 (="10")
    fd = open("/sys/class/gpio/export",O_WRONLY); //  
    write (fd, pOutput, strlen(pOutput));
    close (fd);


    // indicate direction = out
    memset (file, 0, sizeof(file));
    strcat (file, "/sys/class/gpio/gpio");
    strcat (file, pOutput);
    strcat (file, "/direction");  //  file = "/sys/class/gpio/gpio10/direction"
    pDir = "out";
    fd = open(file,O_RDWR);
    write (fd, pDir, strlen(pDir));
    close (fd);


    // Write LED0
    memset (file, 0, sizeof(file));
    strcat (file, "/sys/class/gpio/gpio");
    strcat (file, pOutput);
    strcat (file, "/value"); // file = "/sys/class/gpio/gpio10/value"

    val='0';
    nbByte = 0;
    fd = open(file,O_WRONLY);
    if (fd) {
        nbByte=pwrite (fd, &val, sizeof(val), 0);
        close (fd);
    }
    printf ("nbByte=%d\n", nbByte);


    // Deactivate LED0
    fd = open("/sys/class/gpio/unexport",O_WRONLY); //  "/sys/class/gpio/unexport"
    write (fd, pOutput, strlen(pOutput));
    close (fd);
    
    return (0);
}



