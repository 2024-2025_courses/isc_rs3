// gcc -Wall -Wextra -I /home/schuler/work/mqtt/install/include -o sub sub.c -L /home/schuler/work/mqtt/install/lib -l mosquitto


// API description: https://mosquitto.org/api/files/mosquitto-h.html

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>

static int run = -1;

void on_connect(struct mosquitto *mosq, void *obj, int rc)
{
	if(rc){
		exit(1);
	}else{

		//mosquitto_subscribe (struct	mosquitto *mosq, int *mid, const char *sub,	int qos)
		mosquitto_subscribe(mosq, NULL, "Temperature", 0); // 
	}
}

void on_disconnect(struct mosquitto *mosq, void *obj, int rc)
{
	run = rc;
}


/*
struct mosquitto_message{
	int mid;
	char *topic;
	void *payload;
	int payloadlen;
	int qos;
	bool retain;
};*/

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message * msg)
{
	if (msg == NULL)
	    return;
    printf ("on_message: mid=%d, topic=%s, payload=%s, payload len=%d, qos=%d,  retain=%d\n", msg->mid, msg->topic, (char*)msg->payload, msg->payloadlen, msg->qos, msg->retain);
}


void on_subscribe(struct mosquitto *mosq, void *obj, int mid, int qos_count, const int *granted_qos)
{
    printf ("on_subscribe: mid=%d, qos=%d\n", mid,  *granted_qos);
	//mosquitto_disconnect(mosq);

}

int main(int argc, char *argv[])
{
	int rc;
	struct mosquitto *mosq;

	int port = 1883;

	mosquitto_lib_init();

	mosq = mosquitto_new("Subscriber1", true, NULL); // (client_id, clean_session, obj_for_all_Callback)
	mosquitto_connect_callback_set(mosq, on_connect);
	mosquitto_disconnect_callback_set(mosq, on_disconnect);
	mosquitto_subscribe_callback_set(mosq, on_subscribe);
    mosquitto_message_callback_set(mosq, on_message);

	rc = mosquitto_connect(mosq, "localhost", port, 60);  //n60: keepalive

	while(run == -1){
		mosquitto_loop(mosq, -1, 1);
	}

	mosquitto_lib_cleanup();
	return run;
}
