// gcc -Wall -Wextra -I /home/schuler/work/mqtt/install/include -o pub pub.c -L /home/schuler/work/mqtt/install/lib -l mosquitto


// API description: https://mosquitto.org/api/files/mosquitto-h.html


#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>

static int run = -1;

void on_connect(struct mosquitto *mosq, void *obj, int rc)
{
char *pMessage = "20 degrees";

	if(rc){
		exit(1);
	}else{

        // mosquitto_publish(struct	mosquitto *mosq, int *mid, const char *pub,	int payload_len,, void *payload, int qos, bool retain)
		mosquitto_publish(mosq, NULL, "Temperature", strlen(pMessage), pMessage, 1, false);
	}
}

void on_publish(struct mosquitto *mosq, void *obj, int mid)
{
	mosquitto_disconnect(mosq);
}

void on_disconnect(struct mosquitto *mosq, void *obj, int rc)
{
	run = 0;
}

int main(int argc, char *argv[])
{
int               rc;
struct mosquitto *mosq;
int               port = 1883;
	

	mosquitto_lib_init();

	mosq = mosquitto_new("Publisher1", true, NULL); // (client_id, clean_session, obj_for_all_Callback)
	mosquitto_connect_callback_set(mosq, on_connect);
	mosquitto_disconnect_callback_set(mosq, on_disconnect);
	mosquitto_publish_callback_set(mosq, on_publish);

	rc = mosquitto_connect(mosq, "localhost", port, 60);

	while(run == -1){
		mosquitto_loop(mosq, 300, 1);
	}

	mosquitto_lib_cleanup();
	return run;
}
