// commands: ipcs, ipcrm 


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "my_message.h"



static void catchSignal      (int signal);
static void createCatchSignal(void);


static pthread_t thr_1_ID;
static pthread_t thr_2_ID;


int   id_queue_thr2 = -1;



void *thr_1 (void *arg);
void *thr_2 (void *arg);



int main (void)
{
    void *returnMessage;

    createCatchSignal ();

    /* Create queues  */
    id_queue_thr2 = msgget ((key_t)QUEUE_THR2, 0666 | IPC_CREAT);

    pthread_create (&thr_1_ID, NULL, thr_1, NULL);
    pthread_create (&thr_2_ID, NULL, thr_2, NULL);


    pthread_join (thr_1_ID, &returnMessage);
    pthread_join (thr_2_ID, &returnMessage);
    
    if (id_queue_thr2 != -1)
    {
        msgctl(id_queue_thr2, IPC_RMID, 0);
    }
    
    exit (EXIT_SUCCESS);
}


static void createCatchSignal(void)
{
struct sigaction act;

    act.sa_handler = catchSignal;
    sigemptyset (&act.sa_mask);
    act.sa_flags   = 0;
    sigaction (SIGINT,  &act, 0);
    sigaction (SIGTSTP, &act, 0);
    sigaction (SIGTERM, &act, 0);
    sigaction (SIGABRT, &act, 0);
}


static void catchSignal (int signal)
{
    printf ("signal = %d\n", signal);
    pthread_cancel(thr_1_ID);
    pthread_cancel(thr_2_ID);
}

