// make -f Makefile2

#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>


int main()
{
unsigned char buffer[16];
int           val;
int           nbBytes;


   int fd = open ("/dev/drv2", O_RDWR);
   memset (&buffer[0], 0, sizeof(buffer));

   nbBytes = read (fd, &buffer[0], sizeof(buffer));
   printf ("nbBytes=%d\n", nbBytes);

   val=0;
   nbBytes = write (fd, &val, sizeof(val));
   printf ("nbBytes=%d\n", nbBytes);
   close (fd);



   return 0;
}
