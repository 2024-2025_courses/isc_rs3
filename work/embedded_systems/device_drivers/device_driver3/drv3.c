/***********************************************************************
 *
 * Copyright (C) 2010 by University of Applied Sciences - Fribourg.
 * All rights reserved.
 *
 * $Id: devdrv.c,v 1.0 2010/02/20
 * @Author: Daniel Gachet
 * @Descr: Simple Char Device Driver 
 * @References: [1]
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *  
 ***********************************************************************/

#include <linux/fs.h>	        /* custom configuration */
#include <linux/module.h>	    /* module_init */
#include <linux/init.h>		    /* __init */
#include <linux/cdev.h>         /* cdev */
#include <asm/uaccess.h>        /* copy from/to user methods */

//#include "drv_xyz.h"

#define DRV_MAJOR 42

static int kernelVal = 0;


static int drv_open (struct inode *node, struct file *filp)
{
    printk (KERN_DEBUG "drv: open operation... major:%d, minor:%d\n", 
            imajor (node), iminor(node));
    return 0; /* success */
}


static int drv_release (struct inode *node, struct file *filp)
{
    printk (KERN_DEBUG "drv: release operation...\n");
    return 0; /* success */
}


static ssize_t drv_read(struct file *filp, char __user *buff, 
                        size_t count, loff_t *offp)
{
int tmp;

   
    if (count >= sizeof(kernelVal)) {
        
        if (iminor(filp->f_inode) == 0)
           tmp = kernelVal*2;
        else if (iminor(filp->f_inode) == 1) 
           tmp = kernelVal*3;
        else
           tmp=0;
        

        count = sizeof(tmp);
        if (copy_to_user (buff, &tmp, count))
           return -EFAULT;
    }
    else
        count = 0;

    *offp += count;
    printk (KERN_DEBUG "drv_read: count, kernelVal: %ld, %d\n", count, kernelVal);

    return (count);
}


static ssize_t drv_write(struct file *filp, const char __user *buff, 
                         size_t count, loff_t *offp)
{

    if (count <= sizeof(kernelVal)) {
        if (copy_from_user (&kernelVal, buff, count))
           return -EFAULT;
    }
    else
        count=0;
    *offp += count;

    printk (KERN_DEBUG "drv_write: count, kernelVal: %ld %d\n", count, kernelVal);

    return count;
}


/* char device driver structures & installation methods */
static struct file_operations s_fops = {
    .owner   = THIS_MODULE,
    .read    = drv_read,
    .write   = drv_write,
    .open    = drv_open,
    .release = drv_release,
};

static int __init drv_init_module(void)
{
int status;
    status = register_chrdev(DRV_MAJOR, "drv3", &s_fops);
    
    /* finalize... */
    if (status == 0) 
        printk (KERN_DEBUG "drv3: Simple Char Device Driver loaded\n");
    else
        printk (KERN_DEBUG "drv3: Simple Char Device Driver error while loading the module\n");

    return status;
}

static void __exit drv_cleanup_module(void)
{
    /* remove char device driver */
    unregister_chrdev(DRV_MAJOR, "drv");
    printk (KERN_DEBUG "Simple Char Device Driver unloaded\n");
}

module_init (drv_init_module);
module_exit (drv_cleanup_module);

MODULE_AUTHOR ("Daniel Gachet <daniel.gachet@hefr.ch>, Jean-Roland Schuler <jean-roland.schuler@hefr.ch>");
MODULE_DESCRIPTION ("Simple Char Device Driver Sample Code");
MODULE_LICENSE ("GPL");
