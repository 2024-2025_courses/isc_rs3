#pip install galois

# cd /usr/local/lib/python3.10/dist-packages/galois
# grep -r "def GF" *
#    _fields/_factory.py:def GF(


import galois
gf = galois.GF(3**3, repr="int")        
#gf = galois.GF(3**3, repr="poly")

a = galois.Poly([1,2,2], field=gf)  # a=122 = x^2 + 2x + 2
b = galois.Poly([2,2,2], field=gf)  # a=222 = 2x^2 + 2x + 2
ir= galois.Poly([1,0,0,1], field=gf)# ir=1001 = x^3 + 1
c=a*b
print (c)
print (c%ir)
