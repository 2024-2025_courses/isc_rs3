#pip install galois

# cd /usr/local/lib/python3.10/dist-packages/galois
# grep -r "def GF" *
#    _fields/_factory.py:def GF(


import galois
gf = galois.GF(17**4, repr="int")        
#gf = galois.GF(17**4, repr="poly")

a = galois.Poly([14,3,12,10], field=gf)
b = galois.Poly([4,14,2,9], field=gf)
#ir= galois.Poly([1,0,0,1], field=gf)# ir=1001 = x^4 + 1
c=a+b
print (c)
#print (c%ir)
