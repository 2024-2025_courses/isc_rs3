#pip install galois

# cd /usr/local/lib/python3.10/dist-packages/galois
# grep -r "def GF" *
#    _fields/_factory.py:def GF(


import galois
gf = galois.GF(17**3, repr="int")        
#gf = galois.GF(17**3, repr="poly")

a = galois.Poly([3,6,3], field=gf)
b = galois.Poly([4,0,9], field=gf)
ir= galois.Poly([1,0,0,1], field=gf)# ir=1001 = x^3 + 1
c=a*b
print ("c=", c)
print ("result=", c%ir)

#print (c%ir)
