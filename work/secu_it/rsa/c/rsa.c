// gcc -Wall -Wextra -g -o rsa rsa.c -lcrypto

// see: openssl.../demos/maurices/example2.c

//openssl genrsa -out rsaprivatekey.pem 1024                       // Generate private and public keys
//openssl rsa -in rsaprivatekey.pem -pubout -out rsapublickey.pem  // Get only the public key
//openssl rsa -in rsaprivatekey.pem -text                          // Show the private and public key
//openssl rsa -in rsapublickey.pem -pubin -text                    // Show the public key



//*******************************************************************************
// See: hkey = EVP_PKEY_new_raw_private_key(EVP_PKEY_HMAC, NULL, key, size)
// for raw private or public key

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>

#include <time.h>


//#define LEN_PADDING   50
#define LEN_PADDING_OAEP   41 //50
#define LEN_PADDING_PKCS1  11
#define LEN_PADDING_NO   0

#define LEN_PADDING   LEN_PADDING_PKCS1



static int decryptWithPrivKey (char *pCryptFile, char *pClearFile, char *pPrivKeyFile);
static int cryptWithPubKey    (char *pClearFile, char *pCryptFile, char *pPubKeyFile);



int main()
{
//time_t t1, t2;


    cryptWithPubKey    ("clearFile.txt", "cryptFile.rsa", "rsapublickey.pem");
    decryptWithPrivKey ("cryptFile.rsa", "clearFile1.txt", "rsaprivatekey.pem");

    /*
    time (&t1);
    cryptWithPubKey    ("clearFile.txt", "cryptFile.rsa", "rsapublickey.pem");
    time (&t2);
    printf ("Crypt Pub key, delta T=%ld [s]\n", t2-t1);

    decryptWithPrivKey ("cryptFile.rsa", "clearFile1.txt", "rsaprivatekey.pem");
    */

    //cryptWithPrivKey   ("clearFile.txt", "cryptFile.rsa", "cert.pem");
    //decryptWithPubKey  ("cryptFile.rsa", "clearFile1.txt", "privkey.pem");


    return (0);

}



static int cryptWithPubKey  (char *pClearFile, char *pCryptFile, char *pPubKeyFile)
{
FILE          *pClearFile1;
FILE          *pCryptFile1;
FILE          *pPubKeyFile1;
unsigned char *pBufClear;
unsigned char *pBufCrypt;   
EVP_PKEY      *pPubKey;
int            lenPubKey;
int            lenBufClear;
int            len;
int            len1;


    ERR_load_crypto_strings();

    pPubKey = EVP_PKEY_new();

    pPubKeyFile1 = fopen (pPubKeyFile, "r");   
    PEM_read_PUBKEY(pPubKeyFile1, &pPubKey,0,NULL);
    fclose (pPubKeyFile1);

    lenPubKey   = EVP_PKEY_size(pPubKey);
    lenBufClear = lenPubKey - LEN_PADDING;
    pBufClear   = malloc(lenBufClear);
    pBufCrypt   = malloc(lenPubKey);

    
    pClearFile1 = fopen (pClearFile, "r");
    pCryptFile1 = fopen (pCryptFile, "w");
    
    for (len=lenBufClear; len == lenBufClear; )
    {
        len = fread (pBufClear, 1, lenBufClear, pClearFile1);
        //-----------------------if openssl version > OpenSSL 1.1----------------------------------
        //len1 = RSA_public_encrypt(len, pBufClear, pBufCrypt, EVP_PKEY_get1_RSA(pPubKey), RSA_PKCS1_OAEP_PADDING);
        len1 = RSA_public_encrypt(len, pBufClear, pBufCrypt, EVP_PKEY_get1_RSA(pPubKey), RSA_PKCS1_PADDING);
        //len1 = RSA_public_encrypt(len, pBufClear, pBufCrypt, EVP_PKEY_get1_RSA(pPubKey), RSA_NO_PADDING);



        //-----------------------if openssl version < OpenSSL 1.1----------------------------------
        //len1 = RSA_public_encrypt(len, pBufClear, pBufCrypt, pPubKey->pkey.rsa, RSA_PKCS1_OAEP_PADDING);
        //len1 = RSA_public_encrypt(len, pBufClear, pBufCrypt, pPubKey->pkey.rsa, RSA_PKCS1_PADDING);
        //len1 = RSA_public_encrypt(len, pBufClear, pBufCrypt, pPubKey->pkey.rsa, RSA_NO_PADDING);




        if (len1==lenPubKey)
            fwrite (pBufCrypt, 1, len1, pCryptFile1);
    }     
    
    fclose (pClearFile1);
    fclose (pCryptFile1);
    
    EVP_PKEY_free(pPubKey);
    free (pBufClear);
    free (pBufCrypt);
    ERR_free_strings();
    return (0);
}


static int decryptWithPrivKey (char *pCryptFile, char *pClearFile, char *pPrivKeyFile)
{
FILE          *pClearFile1;
FILE          *pCryptFile1;
FILE          *pPrivKeyFile1;
unsigned char *pBufClear;
unsigned char *pBufCrypt;   
EVP_PKEY      *pPrivKey;
int            lenPrivKey;
int            len;
int            len1;




    ERR_load_crypto_strings();

    pPrivKey = EVP_PKEY_new();

    pPrivKeyFile1 = fopen (pPrivKeyFile, "r");   
    PEM_read_PrivateKey(pPrivKeyFile1, &pPrivKey, 0, NULL);
    fclose (pPrivKeyFile1);

    lenPrivKey = EVP_PKEY_size(pPrivKey);
    pBufCrypt  = malloc(lenPrivKey);
    pBufClear  = malloc(lenPrivKey);

    
    pCryptFile1 = fopen (pCryptFile, "r");
    pClearFile1 = fopen (pClearFile, "w");
    
    for (len=lenPrivKey; len == lenPrivKey; )
    {
        len = fread (pBufCrypt, 1, lenPrivKey, pCryptFile1);
	if (len == lenPrivKey)
	{
            //-----------------------if openssl version > OpenSSL 1.1----------------------------------
	    //len1 = RSA_private_decrypt(len, pBufCrypt, pBufClear, EVP_PKEY_get1_RSA(pPrivKey),RSA_PKCS1_OAEP_PADDING);
	    len1 = RSA_private_decrypt(len, pBufCrypt, pBufClear, EVP_PKEY_get1_RSA(pPrivKey),RSA_PKCS1_PADDING);
	    //len1 = RSA_private_decrypt(len, pBufCrypt, pBufClear, EVP_PKEY_get1_RSA(pPrivKey),RSA_NO_PADDING);

            //---------------------- if openssl version < OpenSSL 1.1-----------------------------
	    //len1 = RSA_private_decrypt(len, pBufCrypt, pBufClear, pPrivKey->pkey.rsa,RSA_PKCS1_OAEP_PADDING);
	    //len1 = RSA_private_decrypt(len, pBufCrypt, pBufClear, pPrivKey->pkey.rsa,RSA_PKCS1_PADDING);
	    //len1 = RSA_private_decrypt(len, pBufCrypt, pBufClear, pPrivKey->pkey.rsa,RSA_NO_PADDING);

	    
            if (len1 >= 0)
               fwrite (pBufClear, 1, len1, pClearFile1);
        }
    }     
    
    fclose (pClearFile1);
    fclose (pCryptFile1);
    

    EVP_PKEY_free(pPrivKey);
    free (pBufCrypt);
    free (pBufClear);
    ERR_free_strings();
    return (0);
}



