
from curve_definition import *

class ecc_curve(object):
    def __init__(self, a, b, p):
        self.a = a
        self.b = b
        self.p = p

    def print_x_y(self):
        for x in range (0,self.p):
            #for y in range (0,self.p):
            for y in range (0,20):
                z=(x**3 +self.a*x + self.b - y**2)%self.p
                if z==0:
                    print ("x= ", x, "y = ", y)


# Paar book: y=(x**3 + 2x + 2) % 17
curve_name = "paarbook1"
#curve_name = "paarbook_prob_97"
#curve_name = "secp256k1_nakov"
a=ECC_CURVES[curve_name]["a"]
b=ECC_CURVES[curve_name]["b"]
p=ECC_CURVES[curve_name]["p"]


ecc = ecc_curve (a=a, b=b, p=p)
ecc.print_x_y()

