


import numpy as np
from matplotlib import pyplot as plt
from curve_definition import *


def equation_draw(a,b,p):
    # Find number of 0
    nbPoint=0
    for x in range (0,p):
        for y in range (0,p):
            z=(x**3 +a*x + b - y**2)%p
            if z==0:
                nbPoint = nbPoint +1

    # Draw the 0s
    x_array=np.arange(nbPoint)
    y_array=np.arange(nbPoint)
    i=0
    for x in range (0,p):
        for y in range (0,p):
            z=(x**3 +a*x + b - y**2)%p
            if z==0:
                x_array[i] = x
                y_array[i] = y
                i=i+1
    
    plt.scatter (x_array,y_array,s=10)
    plt.show()



# Paar book: y=(x**3 + 2x + 2) % 17
curve_name = "paarbook1"
#curve_name = "paarbook_prob_97"
#curve_name = "secp256k1_nakov"
a=ECC_CURVES[curve_name]["a"]
b=ECC_CURVES[curve_name]["b"]
p=ECC_CURVES[curve_name]["p"]

equation_draw (a=a, b=b, p=p)
