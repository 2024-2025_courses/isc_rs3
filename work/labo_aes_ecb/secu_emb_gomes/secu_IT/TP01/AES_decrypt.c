#include <stdio.h>
#include <fcntl.h>
#include <openssl/evp.h>

#define HEADER_AES_H
#define AES_BLOCK_SIZE 16
#define FILE_KEY_SIZE 20

char key_in_file[FILE_KEY_SIZE];
int key_length;
char filename[] = "/workspace/work/secu_it/rootme/mylogin.cnf"; // File to decrypt
char useless[4] = {0};  // Useless data
unsigned char key_after_xor[AES_BLOCK_SIZE] = {0}; // Key after XOR
unsigned char data[96]; // Data to decrypt
unsigned char clear_data[96]; // Decrypted data

int main()
{
    
    FILE *fptr; //Crpyted file

    //Open file
    fptr = fopen(filename, "r");
    if (fptr == NULL)
    {
        perror("Erreur : Open file");
        return 1;
    }

    //Read the first 4 bytes of the file
    fread(useless, 1, sizeof(useless), fptr);

    //Read the key from the file
    fread(key_in_file, 1, sizeof(key_in_file), fptr);

    //XOR the key with the first 16 bytes of the file
    for (size_t i = 0; i < sizeof(key_in_file); i++)
    {
        key_after_xor[i % AES_BLOCK_SIZE] ^= key_in_file[i];   
    }

    //Read data from file
    fread(useless, 1, sizeof(useless), fptr);
    fread(&data, 1, 16, fptr);

    fread(useless, 1, sizeof(useless), fptr);
    fread(&data[16], 1, 16, fptr);

    fread(useless, 1, sizeof(useless), fptr);
    fread(&data[32], 1, 32, fptr);

    fread(useless, 1, sizeof(useless), fptr);
    fread(&data[64], 1, 32, fptr);

    FILE *DecryptFile; //Decrypted file

    EVP_CIPHER_CTX *ctx; //Context for decryption
    int lenPadding = 0;

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    EVP_DecryptInit(ctx, EVP_aes_128_ecb(), key_after_xor, NULL);

    DecryptFile = fopen("decrypted_file.txt", "wb");

    //Decrypt the data
    EVP_DecryptUpdate(ctx, clear_data, &key_length, data, 96);
    fwrite(clear_data, 1, key_length, DecryptFile);
    EVP_DecryptFinal(ctx, clear_data, &lenPadding);
    fwrite(clear_data, 1, lenPadding, DecryptFile);

    //Close files and cleanup
    EVP_CIPHER_CTX_cleanup(ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose(DecryptFile);
    fclose(fptr);
    return 0;
}
