// # gcc -Wall -Wextra -o mycnfdecoder mycnfdecoder.c -lcrypto

// #include <openssl/blowfish.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>

#define BUF_SIZE 128
#define FILENAME "mylogin.cnf"
#define FINALFILENAME "mylogin.decrypted.cnf"
#define uselessByte 4

int main()
{
    FILE *pCryptedFile;
    unsigned char first4Bytes[4];
    unsigned char keyForUncrypt[20];
    unsigned char length[4];
    unsigned char data[BUF_SIZE];
    unsigned char finalKey[16];
    int len;

    // Open the file
    pCryptedFile = fopen(FILENAME, "rb");
    // Set the memory to 0 for each buffer
    memset(&keyForUncrypt[0], 0, sizeof(keyForUncrypt));
    memset(&length[0], 0, sizeof(length));
    memset(&data[0], 0, sizeof(data));
    memset(&first4Bytes[0], 0, sizeof(first4Bytes));
    memset(&finalKey[0], 0, sizeof(finalKey));

    // Read the first 4 useless bytes
    fread(&first4Bytes, 1, uselessByte, pCryptedFile);
    // Read the next 20 bytes to read the key for uncrypt
    fread(&keyForUncrypt[0], 1, sizeof(keyForUncrypt), pCryptedFile);

    // Read the next 4 bytes for the length
    fread(&length[0], 1, sizeof(length), pCryptedFile);
    // Get the lenght who is stored in &length
    len = (length[3] << 24) | (length[2] << 16) | (length[1] << 8) | length[0];
    // Read the first data
    fread(&data[0], 1, len, pCryptedFile);

    // Read the next 4 bytes for the length
    fread(&length[0], 1, sizeof(length), pCryptedFile);
    // Get the lenght who is stored in &length
    len = (length[3] << 24) | (length[2] << 16) | (length[1] << 8) | length[0];
    // Read the second data
    fread(&data[16], 1, len, pCryptedFile);

    // Read the next 4 bytes for the length
    fread(&length[0], 1, sizeof(length), pCryptedFile);
    // Get the lenght who is stored in &length
    len = (length[3] << 24) | (length[2] << 16) | (length[1] << 8) | length[0]; 
    // Read the third data
    fread(&data[32], 1, len, pCryptedFile);

    // Read the next 4 bytes for the length
    fread(&length[0], 1, sizeof(length), pCryptedFile);
    // Get the lenght who is stored in &length
    len = (length[3] << 24) | (length[2] << 16) | (length[1] << 8) | length[0];
    // Read the fourth data
    fread(&data[64], 1, len, pCryptedFile);

    // For each bytes, xor between finalKey and keyForUncrypt to get the initial key
    for (int i = 0; i < 20; i++) {
        finalKey[i % 16] ^= keyForUncrypt[i];
    }

    FILE *pDecryptedFile;
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    // Initialize the decryption
    EVP_DecryptInit(ctx, EVP_aes_128_ecb(), finalKey, NULL);
    // Buffer for data out
    unsigned char dataOut[BUF_SIZE];
    // Decrypt the data
    EVP_DecryptUpdate(ctx, &dataOut[0], &len, &data[0], 64);
    // Write the decrypted data in the file
    pDecryptedFile = fopen(FINALFILENAME, "wb");
    fwrite(&dataOut[0], 1, len, pDecryptedFile);
    // Decrypt the padded data
    int lenPadding = 0;
    EVP_DecryptFinal(ctx, &dataOut[len], &lenPadding);
    // Write the padded data in the file
    fwrite(&dataOut[len], 1, lenPadding, pDecryptedFile);
    //Close the files
    fclose(pCryptedFile);
    fclose(pDecryptedFile);
}
