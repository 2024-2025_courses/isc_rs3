#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define BLOCK_SIZE 16
#define KEY_SIZE 20

void compute_real_key(const unsigned char *key_in_file, unsigned char *real_key){
    // Compute the real key with circular XOR
    for (size_t i = 0; i < KEY_SIZE; i++)
    {
        real_key[i % BLOCK_SIZE] ^= key_in_file[i];
    }
}

int decrypt_mylogin(const char *input_file, const char *output_file){
    FILE *pClearFile;
    FILE *pCryptFile;
    unsigned char cryptBuf[96];
    unsigned char clearBuf[96];
    unsigned char real_key[BLOCK_SIZE] = {0};
    unsigned char key_in_file[KEY_SIZE];
    unsigned char useless[4];
    int nbByteRead;
    int lenCrypt = 0;
    int lenPadding = 0;

    // Open the input file
    pClearFile = fopen(input_file, "rb");

    // Read the key from the file
    fread(useless, 1, 4, pClearFile);  // Skip the first 4 bytes
    fread(key_in_file, 1, sizeof(key_in_file), pClearFile);
    
    // Compute the real key using the key_in_file
    compute_real_key(key_in_file, real_key);

    // Read the encrypted data from the file
    fread(useless, 1, 4, pClearFile);  // Skip the next 4 bytes
    fread(&cryptBuf[0], 1, 16, pClearFile);

    fread(useless, 1, 4, pClearFile);  // Skip the next 4 bytes
    fread(&cryptBuf[16], 1, 16, pClearFile);

    fread(useless, 1, 4, pClearFile);  // Skip the next 4 bytes
    fread(&cryptBuf[32], 1, 32, pClearFile);

    fread(useless, 1, 4, pClearFile);  // Skip the next 4 bytes
    fread(&cryptBuf[64], 1, 32, pClearFile);

    // Initialize decryption context
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    EVP_DecryptInit(ctx, EVP_aes_128_ecb(), real_key, NULL);

    memset(&clearBuf[0], 0, sizeof(clearBuf));

    // Open the output file in write mode
    pCryptFile = fopen(output_file, "wb");

    // Perform decryption
    EVP_DecryptUpdate(ctx, clearBuf, &lenCrypt, cryptBuf, sizeof(cryptBuf));
    fwrite(clearBuf, 1, lenCrypt, pCryptFile);
    EVP_DecryptFinal(ctx, clearBuf, &lenPadding);
    fwrite(clearBuf, 1, lenPadding, pCryptFile);

    // Cleanup
    EVP_CIPHER_CTX_cleanup(ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose(pCryptFile);
    fclose(pClearFile);
    
    return 0;
}

int main(int argc, char *argv[]){
    return decrypt_mylogin(argv[1], argv[2]);
}
