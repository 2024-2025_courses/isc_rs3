#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

#define FINAL_KEY_SIZE 16 // Taille de la clé finale (AES-128)
#define INITIAL_KEY_SIZE 20 // Taille de la clé initiale dans le fichier chiffré
#define MAX_DATA_SIZE 256  // Taille maximale des données chiffrées

// Fonction pour générer la clé finale en XORant les octets de la clé initiale
void generate_final_key(unsigned char *initialKey, unsigned char *finalKey) {
    for (int i = 0; i < INITIAL_KEY_SIZE; i++) {
        finalKey[i % FINAL_KEY_SIZE] ^= initialKey[i]; // XORer les octets de la clé initiale
    }
}

// Fonction pour déchiffrer les données
int decrypt_data(unsigned char *finalKey, unsigned char *encryptedData, size_t totalDataSize, unsigned char *decryptedData) {
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    if (!ctx) {
        printf("Erreur lors de la création du contexte EVP_CIPHER_CTX");
        return -1;
    }

    // Initialiser le contexte de déchiffrement avec l'algorithme AES-128-ECB
    if (EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, finalKey, NULL) != 1) {
        printf("Erreur lors de l'initialisation du déchiffrement");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    int len, lenPadding; // Taille des données déchiffrées et du padding

    // Déchiffrer les données
    if (EVP_DecryptUpdate(ctx, decryptedData, &len, encryptedData, totalDataSize) != 1) {
        printf("Erreur lors de la mise à jour du déchiffrement");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }

    // Finaliser le déchiffrement
    if (EVP_DecryptFinal_ex(ctx, decryptedData + len, &lenPadding) != 1) {
        printf("Erreur lors de la finalisation du déchiffrement");
        EVP_CIPHER_CTX_free(ctx);
        return -1;
    }
    len += lenPadding; // Mettre à jour la taille des données déchiffrées

    EVP_CIPHER_CTX_free(ctx);
    return len;
}

int main(int argc, char *argv[]) {
    if(argc != 2) {
        printf("Usage: %s <key>\n", argv[0]);
        return -1;
    }

    printf("Start the crack of %s\n", argv[1]);

    // Ouvrir le fichier chiffré en mode lecture binaire
    FILE *cryptedFile = fopen(argv[1], "rb");
    if (!cryptedFile) {
        printf("Erreur lors de l'ouverture du fichier");
        return -1;
    }

    unsigned char header[4]; // En-tête du fichier chiffré
    unsigned char initialKey[INITIAL_KEY_SIZE]; // Clé initiale
    unsigned char finalKey[FINAL_KEY_SIZE] = {0}; // Clé finale

    // Lire les premiers 4 octets du fichier
    fread(header, 1, sizeof(header), cryptedFile);

    // Lire les 20 octets suivants (clé initiale)
    fread(initialKey, 1, sizeof(initialKey), cryptedFile);

    // Lire les données chiffrées
    unsigned char length[4]; // Taille des données chiffrées
    unsigned char encryptedData[MAX_DATA_SIZE]; // Données chiffrées
    size_t totalDataSize = 0; // Taille totale des données chiffrées

    // Lire les données chiffrées en segments
    while (fread(length, 1, sizeof(length), cryptedFile) == sizeof(length)) {
        // Taille des données chiffrées
        int len = (length[3] << 24) | (length[2] << 16) | (length[1] << 8) | length[0];

        // Vérifier si la taille dépasse la taille maximale
        if (len <= 0 || totalDataSize + len > MAX_DATA_SIZE) break;

        fread(encryptedData + totalDataSize, 1, len, cryptedFile); // Lire les données chiffrées
        totalDataSize += len; // Mettre à jour la taille totale des données chiffrées
    }

    // Fermer le fichier chiffré
    fclose(cryptedFile);

    // Générer la clé finale
    generate_final_key(initialKey, finalKey);

    // Déchiffrer les données
    unsigned char decryptedData[MAX_DATA_SIZE + EVP_MAX_BLOCK_LENGTH]; // Données déchiffrées
    int finalLen = decrypt_data(finalKey, encryptedData, totalDataSize, decryptedData);
    if (finalLen < 0) {
        return -1;
    }

    // Écrire les données déchiffrées dans un fichier 
    // avec extension [nom de base].decrypted
    char decryptedFilename[256];
    snprintf(decryptedFilename, sizeof(decryptedFilename), "%s.decrypted", argv[1]);

    FILE *decryptedFile = fopen(decryptedFilename, "wb");
    
    if (!decryptedFile) {
        printf("Erreur lors de l'ouverture du fichier de sortie\n");
        return -1;
    }

    fwrite(decryptedData, 1, finalLen, decryptedFile);
    fclose(decryptedFile); // Fermer le fichier de sortie
    printf("Decrypted file available in %s\n", decryptedFilename);
    printf("Job done !\n");

    return 0;
}